# photostream

Personal photostream, because commercial ones have bad UX.

# Getting Started

Dev on windows isn't currently supported due to some tooling that sugars the dev side of things.

For now, I'm assuming you're using macos and homebrew.

## Tooling

I'm assuming you have Go and Node installed.

### Overmind

This handy tool uses the Procfile to run the parcel watcher _and_ the dev server with a single command.

`brew install overmind`

### Fresh

This dead project is the best I've seen for watching go files, and restarting the server. Unfortunately it's no longer maintained.

**NOTE:** Photostream uses go modules, so installing Fresh must be done outside the project directory, or Fresh will be added as a source dependency.

`go get github.com/pilu/fresh`

### The Rest

- `go mod vendor && npm i`
- `npm run dev::watch`
- Dev like a champ
