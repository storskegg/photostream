export interface Gallery {
    name: string
    href: string
    default: boolean
}

export interface GalleryItem {
    src: string
    height: number
    width: number
    srcSet?: any // TODO: CHANGE THIS!
    title?: string
}
