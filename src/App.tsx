console.clear()

import * as React from 'react';
import { render } from 'react-dom';

import PhotoStream from "./PhotoStream";

render(<PhotoStream />, document.getElementById('main'));
