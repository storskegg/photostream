import * as React from "react"
import { Gallery, GalleryItem } from './store/galleries/types'
import NavThing from "./components/NavThing";
import GalleryFeed from "./components/GalleryFeed"

interface Props {}

interface State {
    galleries: Array<Gallery>
    photos: Array<GalleryItem>
}

export default class PhotoStream extends React.Component<Props, State> {
    state: State = {
        galleries: [],
        photos: [],
    }

    h: URL = new URL(document.URL)


    constructor(props: Props | Readonly<Props>) {
        super(props);

        this.h.port = "3001"

        const path = this.h.toString().slice(0, -1)

        fetch(`${path}/api/v1/galleries`)
            .then(res => res.json())
            .then((res) => {
                console.debug('res', res)
                this.setState({
                    galleries: res
                })
            })
            .catch((err) => {
                console.trace(err)
            })

        fetch(`${path}/api/v1/gallery`)
            .then(res => res.json())
            .then((photos) => {
                console.debug('photos', photos)
                this.setState({
                    photos
                })
            })
    }

    render() {
        return <div>
            <NavThing backURL={""} galleries={this.state.galleries} />
            <GalleryFeed photos={this.state.photos} />
        </div>
    }
}
