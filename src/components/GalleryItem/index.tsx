import * as React from "react";
import './galleryItem.scss'

interface Props {
    url: string
    text: string
}

export default class GalleryTile extends React.Component<Props> {
    render() {
        const theStyle = {
            backgroundImage: `url(${this.props.url})`,
            backgroundPosition: 'bottom left',
            backgroundRepeat: 'no-repeat',
        }

        return <div className="gallery-item">
            <div className="bgimg" style={theStyle}>&nbsp;</div>
            <footer>{this.props.text}</footer>
        </div>
    }
}
