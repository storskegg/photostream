import * as React from 'react';

import Count from '../Count'

interface Props {}

interface State {
    count: number
    btnDisabledDecrement: boolean
    btnDisabledIncrement: boolean
}

export default class Counter extends React.Component<Props, State> {
    state: State = {
        count: 5,
        btnDisabledDecrement: false,
        btnDisabledIncrement: false,
    }

    constructor(props) {
        super(props)

        fetch('/api/v1/x')
            .then((res) => res.json())
            .then((res) => {
                const { x } = res
                this.setState({
                    count: x
                })
            })
    }

    increment = () => {
        fetch('/api/v1/x/increment', {method: 'PUT'})
            .then((res) => res.json())
            .then((res) => {
                const { x } = res
                this.setState({
                    count: x
                })
            })
    }

    decrement = () => {
        fetch('/api/v1/x/decrement', {method: 'PUT'})
            .then((res) => res.json())
            .then((res) => {
                const { x } = res
                this.setState({
                    count: x
                })
            })
    }

    render () {
        return (
            <div>
                <Count count={this.state.count} />
                <button onClick={this.increment}>Increment</button>
                <button onClick={this.decrement}>Decrement</button>
            </div>
        )
    }
}