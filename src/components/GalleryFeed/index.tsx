import * as React from "react"
import { memoize } from 'lodash-es/function'
// import Carousel, { Modal, ModalGateway } from "react-responsive-carousel"
import { GalleryItem } from "../../store/galleries/types"
import Gallery from 'react-photo-gallery'
import './galleryFeed.scss'

interface Props {
    photos: Array<GalleryItem>
}

interface State {
    currentImage: number
    viewerIsOpen: boolean
}

export default class GalleryFeed extends React.Component<Props, State> {
    state: State = {
        currentImage: 0,
        viewerIsOpen: false,
    }

    closeLightbox = () => {
        this.setCurrentImage(0)
        this.setViewerIsOpen(false)
    }

    openLightbox = memoize((event, { photo, index }) => {
        this.setCurrentImage(index)
        this.setViewerIsOpen(true)
    }).bind(this)

    setCurrentImage = (id: number) => {
        this.setState({
            currentImage: id
        })
    }

    setViewerIsOpen = (isOpen: boolean) => {
        this.setState({
            viewerIsOpen: isOpen
        })
    }

    render() {
        return <div className="container-gallery">
            <Gallery photos={this.props.photos} onClick={this.openLightbox} />
            {/*<ModalGateway>*/}
            {/*    {this.state.viewerIsOpen ? (*/}
            {/*        <Modal onClose={this.closeLightbox}>*/}
            {/*            <Carousel*/}
            {/*                currentIndex={this.state.currentImage}*/}
            {/*                views={this.props.photos.map(x => ({*/}
            {/*                    ...x,*/}
            {/*                    srcset: x.srcSet,*/}
            {/*                    caption: x.title*/}
            {/*                }))}*/}
            {/*            />*/}
            {/*        </Modal>*/}
            {/*    ) : null}*/}
            {/*</ModalGateway>*/}
        </div>
    }
}
