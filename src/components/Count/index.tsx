import * as React from 'react'
import './count.scss'

interface Props {
    count?: number
}

export default class Count extends React.Component<Props> {
    static defaultProps: Props = {
        count: 10
    };

    render () {
        return <h1 className="count">{this.props.count}</h1>
    }
}
