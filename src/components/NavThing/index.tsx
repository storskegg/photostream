import * as React from 'react'
import { Gallery } from '../../store/galleries/types'
import './navthing.scss'

interface Props {
    backURL: string,
    galleries: Array<Gallery>
}

export default class NavThing extends React.Component<Props> {
    constructor(props: Props | Readonly<Props>) {
        super(props)
    }

    render() {
        return <nav className="masthead">
            <button className="menu-toggle"><span className="icon"></span></button>
            <ul className="menu-items">
                {
                    this.props.backURL == "" ? null : <li><a href={this.props.backURL}>Back... '{this.props.backURL}'</a></li>
                }
                {
                    this.props.galleries.map((gallery, idx) => {
                        const { name, href } = gallery
                        const active = gallery.default ? 'active' : ''

                        return <li className={active} key={idx}><a href={href}>{name}</a></li>
                    })
                }
            </ul>
            <div className="my-name">{'{Storskegg}org'}</div>
        </nav>
    }
}
