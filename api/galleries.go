package api

import (
	"encoding/json"
	"log"
	"net/http"
)

type GalleryLink struct {
	Name    string `json:"name"`
	Href    string `json:"href"`
	Default bool   `json:"default"`
}

var galleries = []GalleryLink{
	{
		"Gallery 1",
		"galleries/gallery1",
		true,
	},
	{
		"Gallery 2",
		"galleries/gallery2",
		false,
	},
}

func Galleries(w http.ResponseWriter, r *http.Request) {
	// TODO: make this actually do something
	err := json.NewEncoder(w).Encode(galleries)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		log.Println(err)
	}
}
