package api

import (
	"encoding/json"
	"log"
	"net/http"
)

func Tags(w http.ResponseWriter, r *http.Request) {
	// TODO: make this actually do something
	err := json.NewEncoder(w).Encode(map[string]bool{"ok": true})
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		log.Println(err)
	}
}
