package api

import (
	"encoding/json"
	"fmt"
	"log"
	"math/rand"
	"net/http"
)

type GalleryItem struct {
	Src    string `json:"src"`
	Height int    `json:"height"`
	Width  int    `json:"width"`
}

func Gallery(w http.ResponseWriter, r *http.Request) {
	_set := make([]GalleryItem, 0)

	for i := 0; i < 20; i++ {
		x := randGalleryDim(3)
		y := randGalleryDim(3)
		src := fmt.Sprintf("https://picsum.photos/id/%d/%d/%d", randGalleryDim(500), x*300, y*300)
		_set = append(_set, GalleryItem{src, y, x})
	}

	err := json.NewEncoder(w).Encode(_set)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		log.Println(err)
	}
}

func randGalleryDim(max int) (n int) {
	return rand.Intn(max-1) + 2
}
