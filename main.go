package main

import (
	"context"
	"flag"
	"fmt"
	"log"
	"net/http"
	"net/url"
	"os"
	"os/exec"
	"os/signal"
	"runtime"
	"syscall"
	"time"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"github.com/urfave/cli/v2"
	"gitlab.com/photostream/api"
	"gitlab.com/photostream/serveSPA"
)

func init() {
	log.Println("Initializing global variables")

	log.Println("Globals initialized")
}

func main() {
	app := &cli.App{
		Name:   "Photostream",
		Action: start,
		Flags: []cli.Flag{
			&cli.StringFlag{
				Name:    "NODE_ENV",
				EnvVars: []string{"NODE_ENV"},
				Hidden:  true,
				Value:   "production",
			},
		},
	}

	err := app.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}
}

func start(c *cli.Context) error {
	var wait time.Duration
	flag.DurationVar(&wait, "graceful-timeout", time.Second*15, "the duration for which the server gracefully wait for existing connections to finish - e.g. 15s or 1m")
	flag.Parse()

	r := mux.NewRouter()
	r.Use(handlers.RecoveryHandler()) // Handle panics with grace
	r.Use(handlers.CompressHandler)   // A little GZip goes a long way

	r.HandleFunc("/sk/status", func(w http.ResponseWriter, r *http.Request) {
		// an example API handler
		_, err := w.Write([]byte("OK"))
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			log.Println(err)
		}
	})

	r.HandleFunc("/api/v1/galleries", api.Galleries)
	r.HandleFunc("/api/v1/gallery", api.Gallery)
	r.HandleFunc("/api/v1/tags", api.Tags)

	spa := serveSPA.SpaHandler{StaticPath: "dist", IndexPath: "index.html"}
	r.PathPrefix("/").Handler(spa)

	originValidator := handlers.AllowedOriginValidator(func(origin string) bool {
		u, err := url.ParseRequestURI(origin)
		if err != nil {
			log.Println("ERR parsing origin: ", origin)
			return false
		}

		if u.Hostname() != "localhost" {
			log.Println("REJECTED: Origin hostname: ", u.Hostname())
			return false
		}

		switch u.Port() {
		case "1234", "3001":
			log.Println("ACCEPTED: Origin port: ", u.Port())
			return true
		default:
			log.Println("REJECTED: Origin port: ", u.Port())
			return false
		}
	})

	//allowedOrigins := handlers.AllowedOrigins([]string{
	//	"localhost:3001",
	//	"localhost:1234",
	//	"*",
	//})

	srv := &http.Server{
		Handler: handlers.CORS(
			originValidator,
			handlers.AllowCredentials(),
			handlers.IgnoreOptions(),
			handlers.AllowedMethods([]string{
				http.MethodGet,
				http.MethodHead,
				http.MethodPost,
				http.MethodPut,
				http.MethodDelete,
			}),
		)(r),
		Addr: ":3001",
		// Good practice: enforce timeouts for servers you create!
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
		IdleTimeout:  60 * time.Second,
	}

	go func() {
		log.Printf("Listening on %s", srv.Addr)
		openIfNotOpen(fmt.Sprintf("http://127.0.0.1:%s", srv.Addr), c)
		if err := srv.ListenAndServe(); err != nil {
			log.Print(err)
		}
	}()

	chanSig := make(chan os.Signal, 1)

	// We'll always attempt graceful shutdowns when quit via SIGINT (Ctrl+C), KILL, QUIT or TERM
	signal.Notify(chanSig, syscall.SIGINT, syscall.SIGQUIT, syscall.SIGTERM)

	// Block until we receive our signal.
	<-chanSig

	// Create a deadline to wait for.
	ctx, cancel := context.WithTimeout(context.Background(), wait)
	defer cancel()
	// Doesn't block if no connections, but will otherwise wait
	// until the timeout deadline.
	err := srv.Shutdown(ctx)
	if err != nil {
		log.Panic(err)
	}
	// Optionally, you could run srv.Shutdown in a goroutine and block on
	// <-ctx.Done() if your application should wait for other services
	// to finalize based on context cancellation.
	return cli.Exit("Graceful Shutdown", 0)
}

//////////////////////////////////////////////////////////////////////
// PRIVATE METHODS (Mostly for dev purposes)

func openIfNotOpen(url string, c *cli.Context) {
	if c.String("NODE_ENV") != "development" {
		return
	}
	_, err := os.Stat("./.browser.lock")
	if err == nil {
		log.Print("Browser lock detected")
		return
	}
	_, err = os.Create("./.browser.lock")
	if err != nil {
		log.Fatal(err)
		return
	}
	time.Sleep(5000 * time.Millisecond)
	err = open(url)
	if err != nil {
		log.Fatal(err)
	}
}

// open opens the specified URL in the default browser of the user.
func open(url string) error {
	var cmd string
	var args []string

	switch runtime.GOOS {
	case "windows":
		cmd = "cmd"
		args = []string{"/c", "start"}
	case "darwin":
		cmd = "open"
	default: // "linux", "freebsd", "openbsd", "netbsd"
		cmd = "xdg-open"
	}
	args = append(args, url)
	return exec.Command(cmd, args...).Start()
}
